<!--
SPDX-FileCopyrightText: 2022 Thomas Kramer

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Verilog Netlist I/O for LibrEDA

This crate implements a `NetlistReader` and `NetlistWriter` of the LibrEDA framework for the Verilog netlist format used by Yosys.

Only a subset of Verilog is supported, namely 'structural' or 'netlist' Verilog. Which consists only of modules,
module instantiations and port connections.
