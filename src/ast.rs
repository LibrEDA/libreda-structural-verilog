// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! This module defines data structures for the abstract syntax tree which is used
//! to represent the Verilog file.
//!
//! # References
//!
//! <https://www.verilog.com/VerilogBNF.html>

use std::num::ParseIntError;

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Ident(pub String);

impl Ident {
    pub fn name(&self) -> &String {
        &self.0
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum Expr {
    Ref(Ident),
    IdentIndexed(IdentIndexed),
    IdentSliced(IdentSliced),
    /// Integer value with optional bit length.
    Num(Num),
    /// Something like `{a, b, c}`.
    Concatenation(Vec<Expr>),
}

impl Expr {
    /// Return a flat list of all expressions of a possible concatenation.
    pub fn flat_concatenation(&self) -> Vec<&Expr> {
        let mut v = Vec::new();

        fn flatten<'a>(v: &mut Vec<&'a Expr>, e: &'a Expr) {
            match e {
                Expr::Concatenation(c) => c.iter().for_each(|e| flatten(v, e)),
                _ => v.push(e),
            }
        }

        flatten(&mut v, self);

        v
    }
}

/// Integer value with optional bit length.
#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
pub struct Num {
    /// Integer value without any x or z.
    pub value: u64,
    /// Don't care (x) bitmap.
    pub dont_care: u64,
    /// High-impedance (z) bitmap.
    pub high_impedance: u64,
    /// Bit length of the number.
    pub bitlength: Option<u64>,
}

/// 4-valued logic bit.
#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
pub enum Bit {
    /// Logic zero.
    Zero,
    /// Logic one.
    One,
    /// Don't care (x).
    DontCare,
    /// High impedance (z).
    HighImpedance,
}

impl Num {
    /// Create a number without any x or z bits.
    pub fn normal(value: u64, bitlen: Option<u64>) -> Self {
        Num {
            value,
            dont_care: 0,
            high_impedance: 0,
            bitlength: bitlen,
        }
    }

    /// Create a `Num` from a string.
    pub fn parse(s: &str, radix: u32, bitlen: Option<u64>) -> Result<Self, ParseIntError> {
        let (value, x, z) = parse_xz_integer(s, radix)?;
        Ok(Num {
            value,
            dont_care: x,
            high_impedance: z,
            bitlength: bitlen,
        })
    }

    /// Convert the integer to a bit vector starting with the least significant bit.
    pub fn to_bits_lsb_first(&self) -> Vec<Bit> {
        let mut value = self.value;
        let mut xs = self.dont_care;
        let mut zs = self.high_impedance;

        let mut bits = Vec::new();

        while value | xs | zs > 0 {
            let b = (value & 0b1) as u8;
            let x = (xs & 0b1) as u8;
            let z = (zs & 0b1) as u8;
            let bit = match (b, x, z) {
                (0, 0, 0) => Bit::Zero,
                (1, 0, 0) => Bit::One,
                (_, 1, 0) => Bit::DontCare,
                (_, 0, 1) => Bit::HighImpedance,
                _ => panic!("Invalid state."),
            };
            bits.push(bit);
            value = value >> 1;
            xs = xs >> 1;
            zs = zs >> 1;
        }

        if let Some(bitlen) = self.bitlength {
            bits.resize(bitlen as usize, Bit::Zero);
        }

        bits
    }

    /// Convert the integer to a bit vector starting with the most significant bit.
    pub fn to_bits_msb_first(&self) -> Vec<Bit> {
        let mut bits = self.to_bits_lsb_first();
        bits.reverse();
        bits
    }

    /// Check if there is no X or Z bits set.
    pub fn is_normal(&self) -> bool {
        self.dont_care == 0 && self.high_impedance == 0
    }
}

#[test]
fn test_num_to_bits() {
    let _0 = Bit::Zero;
    let _1 = Bit::One;
    assert_eq!(Num::normal(6, None).to_bits_msb_first(), vec![_1, _1, _0]);
    assert_eq!(Num::normal(6, None).to_bits_lsb_first(), vec![_0, _1, _1]);
    assert_eq!(
        Num::normal(6, Some(4)).to_bits_msb_first(),
        vec![_0, _1, _1, _0]
    );
    assert_eq!(Num::normal(6, Some(2)).to_bits_msb_first(), vec![_1, _0]);
}

/// Parse an integer with `don't care` and `high-impedance` bits set.
/// Something like `12abxXzZ`. Radix 2, 8 and 16 are supported.
///
/// Returns a tuple (value without any x or z, x-bitmask, z-bitmask).
fn parse_xz_integer(s: &str, radix: u32) -> Result<(u64, u64, u64), ParseIntError> {
    let is_x = |c: char| -> bool { c == 'x' || c == 'X' };
    let is_z = |c: char| -> bool { c == 'z' || c == 'Z' };
    let is_xz = |c: char| -> bool { is_x(c) || is_z(c) };

    let one_digit = match radix {
        2 => "1",
        8 => "7",
        16 => "f",
        _ => panic!("Unsupported radix."),
    };

    let non_xz = s.replace(&is_xz, "0");
    let x = s.replace(|c| !is_x(c), "0").replace(&is_x, one_digit);
    let z = s.replace(|c| !is_z(c), "0").replace(&is_z, one_digit);

    let i_non_xz = u64::from_str_radix(non_xz.as_str(), radix)?;
    let i_x = u64::from_str_radix(x.as_str(), radix)?;
    let i_z = u64::from_str_radix(z.as_str(), radix)?;

    Ok((i_non_xz, i_x, i_z))
}

#[test]
fn test_parse_xz_integer() {
    assert_eq!(
        parse_xz_integer("10xzXZ", 2),
        Ok((0b100000, 0b1010, 0b0101))
    );
    assert_eq!(
        parse_xz_integer("10xzXZ", 8),
        Ok((0o100000, 0o7070, 0o0707))
    );
    assert_eq!(
        parse_xz_integer("10xzXZ", 16),
        Ok((0x100000, 0xf0f0, 0x0f0f))
    );
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Module {
    pub name: Ident,
    pub ports: Vec<Port>,
    pub body: Vec<ModuleItem>,
}

impl Module {
    /// Iterate over (module name, instance) pairs for all instances in this module.
    pub fn instantiations(&self) -> impl Iterator<Item = (&String, &ModuleInstance)> {
        self.body
            .iter()
            .filter_map(|item| match item {
                ModuleItem::ModuleInstantiation(name, insts) => {
                    Some(insts.iter().map(move |inst| (name.name(), inst)))
                }
                _ => None,
            })
            .flatten()
    }

    /// Iterate over all continuous assignments.
    pub fn continuous_assignments(&self) -> impl Iterator<Item = &Assignment> {
        self.body
            .iter()
            .filter_map(|item| match item {
                ModuleItem::ContinuousAssign(c) => Some(c.iter()),
                _ => None,
            })
            .flatten()
    }

    /// Iterate over all IO declarations as (slice operator, name) pairs.
    pub fn all_io_declarations(
        &self,
    ) -> impl Iterator<Item = (&Option<SliceOp>, &String, Direction)> {
        self.body
            .iter()
            .filter_map(|item| match item {
                ModuleItem::IODeclaration(slice, names, dir) => {
                    Some(names.iter().map(move |n| (slice, &n.0, *dir)))
                }
                _ => None,
            })
            .flatten()
    }

    // /// Iterate over all IO declarations as (slice operator, name) pairs filtered by the direction.
    // pub fn io_declarations(&self, direction: Direction) -> impl Iterator<Item=(&Option<SliceOp>, &String)> {
    //     self.body.iter()
    //         .filter_map(move |item| match item {
    //             ModuleItem::IODeclaration(slice, names, dir)
    //             if *dir == direction => {
    //                 Some(names.iter().map(move |n| (slice, &n.0)))
    //             }
    //             _ => None
    //         })
    //         .flatten()
    // }

    // /// Iterate over all output declarations as (slice operator, name) pairs.
    // pub fn input_declarations(&self) -> impl Iterator<Item=(&Option<SliceOp>, &String)> {
    //     self.io_declarations(Direction::Input)
    // }

    // /// Iterate over all inout declarations as (slice operator, name) pairs.
    // pub fn inout_declarations(&self) -> impl Iterator<Item=(&Option<SliceOp>, &String)> {
    //     self.io_declarations(Direction::InOut)
    // }
    //
    // /// Iterate over all input declarations as (slice operator, name) pairs.
    // pub fn output_declarations(&self) -> impl Iterator<Item=(&Option<SliceOp>, &String)> {
    //     self.io_declarations(Direction::Output)
    // }

    /// Iterate over all net declarations as (slice operator, name) pairs.
    pub fn net_declarations(&self) -> impl Iterator<Item = (&Option<SliceOp>, &String)> {
        self.body
            .iter()
            .filter_map(|item| match item {
                ModuleItem::NetDeclaration(slice, names) => {
                    Some(names.iter().map(move |n| (slice, &n.0)))
                }
                _ => None,
            })
            .flatten()
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum Port {
    Ref(Ident),
    Named(NamedPortConnection),
}

/// Port connection of the style `.portName(expression)`.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct NamedPortConnection(pub Ident, pub Expr);

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum ModulePortConnections {
    /// Connections by position.
    Unnamed(Vec<Expr>),
    /// Connections by port name.
    Named(Vec<NamedPortConnection>),
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct ModuleInstance(
    /// Instance name.
    pub Ident,
    pub ModulePortConnections,
);

/// A range like `[start : end]`.
#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
pub struct SliceOp(pub Num, pub Num);

impl SliceOp {
    pub fn highest_index(&self) -> u64 {
        self.0.value
    }

    pub fn lowest_index(&self) -> u64 {
        self.1.value
    }

    /// Expand the indices of this slice.
    /// A slice `[3:0]` will expand to `[0, 1, 2, 3]`.
    pub fn indices_range(&self) -> impl Iterator<Item = usize> {
        let range = self.lowest_index()..self.highest_index() + 1;
        range.into_iter().rev().map(|i| i as usize)
    }

    /// Get the bit-width of the slice.
    pub fn width(&self) -> usize {
        (1 + self.highest_index() - self.lowest_index()) as usize
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct IndexOp(pub Num);

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct IdentIndexed(pub Ident, pub IndexOp);

impl IdentIndexed {
    pub fn name(&self) -> &String {
        self.0.name()
    }
    pub fn index(&self) -> usize {
        self.1 .0.value as usize
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct IdentSliced(pub Ident, pub SliceOp);

impl IdentSliced {
    pub fn name(&self) -> &String {
        self.0.name()
    }

    pub fn indices(&self) -> Vec<usize> {
        self.1.indices_range().collect()
    }

    pub fn highest_index(&self) -> usize {
        self.1.highest_index() as usize
    }

    pub fn lowest_index(&self) -> usize {
        self.1.lowest_index() as usize
    }
}

/// 'Left' value of an assignment.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum LValue {
    Ident(Ident),
    IdentIndexed(IdentIndexed),
    IdentSliced(IdentSliced),
    Concatenation(Vec<LValue>),
}

impl LValue {
    pub fn flat_concatenation(&self) -> Vec<&LValue> {
        let mut v = Vec::new();

        fn flatten<'a>(v: &mut Vec<&'a LValue>, e: &'a LValue) {
            match e {
                LValue::Concatenation(c) => c.iter().for_each(|e| flatten(v, e)),
                _ => v.push(e),
            }
        }

        flatten(&mut v, self);

        v
    }

    /// Convert LValue to an expression.
    pub fn to_expr(&self) -> Expr {
        match self {
            LValue::Ident(i) => Expr::Ref(i.clone()),
            LValue::IdentIndexed(i) => Expr::IdentIndexed(i.clone()),
            LValue::IdentSliced(i) => Expr::IdentSliced(i.clone()),
            LValue::Concatenation(c) => {
                Expr::Concatenation(c.iter().map(|l| l.to_expr()).collect())
            }
        }
    }
}

/// Assignment of the form `left = right`.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Assignment(pub LValue, pub Expr);

#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
pub enum Direction {
    Input,
    Output,
    InOut,
}

/// Item within the module.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum ModuleItem {
    /// Range with list of variables.
    /// Something like `input [start:end] name;`.
    IODeclaration(Option<SliceOp>, Vec<Ident>, Direction),
    /// Something like `wire [start:end] name;`.
    NetDeclaration(Option<SliceOp>, Vec<Ident>),
    /// Something like `assign left = right;`.
    ContinuousAssign(Vec<Assignment>),
    /// Module name and instances.
    ModuleInstantiation(Ident, Vec<ModuleInstance>),
}
