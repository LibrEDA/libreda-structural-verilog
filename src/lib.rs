// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! This crate provides serialization and deserialization of netlists into the Verilog format used
//! by Yosys. Reader and writer implement the `NetlistReader` and `NetlistWriter` traits of the LibrEDA data base.
//!
//! # Examples
//!
//! ## Read a netlist
//!
//! A typical netlist uses standard-cells from a library, hence the standard-cells definitions
//! are not contained in the netlist itself but in an other file.
//!
//! In this example, the library netlist is read first such that all standard-cell definitions are loaded
//! Then the netlist with the actual circuit is loaded.
//!
//! ```
//! use std::fs::File;
//! use libreda_db::prelude::*;
//! use libreda_structural_verilog::StructuralVerilogReader;
//!
//! // Locations of the library and the netlist.
//! let library_path = "./tests/test_data/standard_cells.v";
//! let file_path = "./tests/test_data/my_chip_45_nl.v";
//! let mut f_library = File::open(library_path).unwrap();
//! let mut f_netlist = File::open(file_path).unwrap();
//!
//! // Create an empty netlist that will be populated from files.
//! let mut netlist = Chip::new();
//!
//! // Create a reader that reads only cell definitions but does not care about
//! // the internals of the cell.
//! let library_reader = StructuralVerilogReader::new()
//!         .load_blackboxes(true);
//!
//! // Read the library.
//! library_reader.read_into_netlist(&mut f_library, &mut netlist).expect("Error while reading library.");
//!
//! // Read the netlist with a default reader (does not load black-boxes but populates the cells with content).
//! let reader = StructuralVerilogReader::new();
//! reader.read_into_netlist(&mut f_netlist, &mut netlist).expect("Error while reading netlist.");
//! ```

#![deny(missing_docs)]
#![deny(unused_imports)]

mod ast;
mod reader;
mod writer;

pub use reader::{ParseError, StructuralVerilogReader};
pub use writer::{StructuralVerilogWriter, VerilogWriteError};
