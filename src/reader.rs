// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! This module implements a netlist reader for structural Verilog.

use itertools::Itertools;
use lalrpop_util::lalrpop_mod;

use libreda_db::core::prelude::{Direction, NetlistBase, NetlistEdit, NetlistReader};

use libreda_db::netlist::util::*;
use petgraph::prelude::GraphMap;
use std::collections::{HashMap, HashSet};
use std::io::Read;

use crate::ast;
use crate::ast::{Bit, Expr, Module, ModulePortConnections, Port};
use std::borrow::Borrow;

// Import the parser synthesized by LALRPOP.
lalrpop_mod!(pub grammar);

/// Error type returned by the Verilog reader.
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum ParseError {
    /// Some unspecified error.
    UndefinedError(String),
    /// Modules have a cyclic dependency. (Recursive module instantiation)
    CyclicDependency,
    /// The Verilog data is not UTF-8 encoded.
    InvalidUTF8,
    /// The syntax is not not accepted.
    /// Contains an error message string.
    InvalidVerilog(String),
    /// Some modules are not known yet. Contains names of the missing modules.
    MissingModules(Vec<String>),
    /// Some module ports are neither declared as input nor output.
    /// Contains module name and list of affected module ports.
    PortsWithoutDirection(String, Vec<String>),
    /// Some nets are used but not declared with a `wire`/`input`/`output`/`inout` statement.
    UndeclaredNets(Vec<String>),
    /// Bit width is wrong in an assignment.
    AssignmentWidthMismatch(Vec<(ast::LValue, ast::Expr)>),
    /// Module with given name has no port with the given name.
    NoSuchPort(String, String),
    /// Mismatch of number of ports in port connection of a module instantiation.
    PortConnectionMismatch,
    /// Bit-width mismatch in a port connection.
    PortConnectionWidthMismatch,
}

#[test]
fn test_parse_verilog() {
    let test_verilog_code: &str = r"

/* Header comment ( test ) */

module OR(A, B);
    input A, B;
endmodule

module NOT(A);
    input A;
endmodule

module blabla(port1, port_2);
    input [0:1234] asdf;
    output [1:3] qwer;
    wire [1234:45] mywire;

    wire \escaped_name[[a]} ;

    assign a = b;

    assign a = 8'h12xX;
    assign b = 8'h12zZ;
    assign c = 8'b00010010xxXX;
    assign d = 8'b00010010zzZZ;

    assign {a, b[1], c[0: 39]} = {x, y[5], z[1:40]};
    assign {a, b[1], c[0: 39]} = {x, y[5], 1'h0 };
    (* asdjfasld ajsewkea 3903na ;lds *)
    wire zero_set;
    OR _blabla_ ( .A(netname), .B (qwer) );
    OR blabla2 ( .A(netname), .B (1'b0) );

    wire zero_res;
    (* src = 'alu_shift.v:23' *)
    wire zero_set;
    NOT #(
            .param1(1'h0),
            .param2(1'h1)
            )
        _072_ (
        .A(func_i[2]),
        .Y(_008_)
    );

    // Use a module before it is defined.
    DefinedAfterUsage instance_used_before_definition (1'b0);

endmodule

module DefinedAfterUsage(A);
    input A;
endmodule
";

    let parser = grammar::NetlistParser::new();

    log::debug!("Parse structural verilog.");
    let result: Result<Vec<Module>, _> = parser.parse(test_verilog_code);
    dbg!(&result);
    assert!(result.is_ok());
}

/// Reader for purely structural Verilog.
/// Only very basic Verilog features are supported.
pub struct StructuralVerilogReader {
    /// If set to true cells will be read but not populated with internal instances.
    load_blackboxes: bool,
}
struct Context<'a> {
    modules_by_name: &'a HashMap<&'a String, &'a Module>,
    /// Bit-widths of ports for all modules.
    /// This is updated while each module is processed.
    /// Since modules are processed in bottom-up order all required information should
    /// be here on time.
    /// module name -> (port name -> port width)
    port_widths_by_module: HashMap<&'a String, HashMap<&'a String, usize>>,
    port_names_by_module: HashMap<&'a String, Vec<&'a String>>,
    /// Tells for each module how multi-bit pins have been expanded.
    /// module name -> (port name -> pin position)
    pin_expansion: HashMap<&'a String, HashMap<&'a String, Vec<usize>>>,
}

impl StructuralVerilogReader {
    /// Create a default Verilog reader.
    pub fn new() -> Self {
        StructuralVerilogReader {
            load_blackboxes: false,
        }
    }

    /// If set to true cells will be read but not populated with internal instances.
    /// This way the reader will read only cell definitions but does not care about
    /// the internals of the cell. This is useful for loading libraries of standard-cells
    /// where the content of the standard-cell is not needed.
    pub fn load_blackboxes(mut self, load_blackboxes: bool) -> Self {
        self.load_blackboxes = load_blackboxes;
        self
    }

    /// Populate a netlist based on the Verilog abstract syntax tree.
    fn convert_ast_to_netlist<N: NetlistEdit>(
        &self,
        modules: Vec<ast::Module>,
        netlist: &mut N,
    ) -> Result<(), ParseError> {
        log::debug!("Convert Verilog abstract syntax tree into a netlist.");

        // Build module dependency tree.
        // This helps to determine the correct order for creating the circuits in the netlist.

        let dependency_graph = self.build_module_dependency_graph(&modules);

        // Sort modules topologically. Each module is ordered before its successors.
        log::debug!("Sort modules topologically.");
        let topo_sorted = topo_sort(dependency_graph)?;

        // Create lookup table to find modules by name.
        let modules_by_name: HashMap<_, _> = modules
            .iter()
            .map(|module| (module.name.name(), module))
            .collect();

        // Check that all modules are present.
        check_missing_modules(&topo_sorted, &modules_by_name, netlist)?;

        // Create circuits.

        // Create only circuits that appear in this verilog file.
        // Other circuits might be already present in the netlist.
        let modules_to_create = topo_sorted
            .iter()
            .filter(
                |&name| {
                    modules_by_name.contains_key(name)
                        && netlist.cell_by_name(name.as_str()).is_none()
                }, // Do not create a circuit when it already exists.
            )
            .collect_vec();

        let mut context = Context {
            modules_by_name: &modules_by_name,
            port_widths_by_module: Default::default(),
            port_names_by_module: Default::default(),
            pin_expansion: Default::default(),
        };

        for &v_module_name in modules_to_create {
            self.build_module(v_module_name, netlist, &mut context)?;
        }

        Ok(())
    }

    fn build_module<'a, N: NetlistEdit>(
        &self,
        v_module_name: &'a String,
        netlist: &mut N,
        context: &mut Context<'a>,
    ) -> Result<(), ParseError> {
        let modules_by_name = &context.modules_by_name;
        let port_widths_by_module = &mut context.port_widths_by_module;
        let port_names_by_module = &mut context.port_names_by_module;
        let pin_expansion = &mut context.pin_expansion;
        let v_module = &modules_by_name[v_module_name];
        log::trace!("Create module: '{}'", v_module_name);
        assert!(
            netlist.cell_by_name(v_module_name.as_str()).is_none(),
            "Circuit is already defined."
        );
        let wire_nets: HashSet<_> = v_module.net_declarations().collect();
        let io_nets: HashSet<_> = v_module
            .all_io_declarations()
            .map(|(slice, name, _dir)| (slice, name))
            .collect();
        let io_net_directions_by_name: HashMap<_, _> = v_module
            .all_io_declarations()
            .map(|(_slice, name, dir)| (name, dir))
            .collect();
        let all_net_declarations = wire_nets
            .iter()
            .chain(io_nets.iter())
            .copied()
            .collect_vec();
        let net_declaration_by_name: HashMap<_, _> = all_net_declarations
            .iter()
            .map(|&(slice, name)| (name, slice))
            .collect();
        log::trace!("Number of net names: {}", net_declaration_by_name.len());
        let signal_width = |name: &String| -> usize {
            net_declaration_by_name[name]
                .map(|slice| slice.width())
                .unwrap_or(1)
        };

        let io_net_names: HashSet<&String> = io_nets.iter().map(|&(_, name)| name).collect();
        let wire_net_names: HashSet<&String> = wire_nets.iter().map(|&(_, name)| name).collect();
        let port_names = v_module
            .ports
            .iter()
            .map(|port| match port {
                Port::Ref(ident) => ident.name(),
                Port::Named(name) => name.0.name(),
            })
            .collect_vec();
        {
            let port_names_set: HashSet<_> = port_names.iter().copied().collect();

            {
                // Sanity check: check that every port name is declared as input, output or inout.
                log::trace!(
                    "Check that every port name is declared as a input/output/inout signal."
                );

                let ports_without_direction: HashSet<_> =
                    port_names_set.difference(&io_net_names).copied().collect();
                if !ports_without_direction.is_empty() {
                    log::error!("Found ports in module {} that are neither declared as input nor output: {}",
                                v_module_name, ports_without_direction.iter().sorted().join(", "));
                    return Err(ParseError::PortsWithoutDirection(
                        v_module_name.clone(),
                        ports_without_direction.into_iter().cloned().collect(),
                    )); // TODO: More specific error type.
                }
            }

            {
                // Sanity check: check that every input/output declaration is actually a port name.
                log::debug!(
                    "Check that every input/output/inout declaration is actually a port name."
                );

                let unnecessary_io_declarations: HashSet<_> =
                    io_net_names.difference(&port_names_set).collect();

                if !unnecessary_io_declarations.is_empty() {
                    log::error!(
                        "Net declared as input/output but not in port list: {}",
                        unnecessary_io_declarations.iter().sorted().join(", ")
                    );
                    // TODO: Make configurable to return an Err?
                }
            }

            {
                // Sanity check: All used nets should be declared.
                log::debug!("Check that all used nets are declared.");

                // Get all nets names that are connected to module instances.
                let module_instance_nets: HashSet<_> = v_module
                    .instantiations()
                    .flat_map(|(_, inst)| match &inst.1 {
                        ModulePortConnections::Unnamed(unnamed) => {
                            unnamed.iter().collect_vec().into_iter()
                        }
                        ModulePortConnections::Named(named) => {
                            named.iter().map(|named| &named.1).collect_vec().into_iter()
                        }
                    })
                    .flat_map(|expr| expr.flat_concatenation().into_iter())
                    .filter_map(|expr| match expr {
                        Expr::Ref(i) => Some(i.name()),
                        Expr::IdentIndexed(i) => Some(i.name()),
                        Expr::IdentSliced(i) => Some(i.name()),
                        Expr::Num(_) => None, // Ignore constants.
                        Expr::Concatenation(_) => {
                            panic!("Flattened concatenation should not contain a concatenation.")
                        }
                    })
                    .collect();

                // Find nets that are used but not declared.
                let undeclared_nets: HashSet<&String> = module_instance_nets
                    .difference(&wire_net_names.union(&io_net_names).copied().collect())
                    .copied()
                    .collect();

                if !undeclared_nets.is_empty() {
                    let nets = undeclared_nets.into_iter().cloned().collect_vec();
                    log::error!("Found undeclared nets: {}.", nets.join(", "));
                    return Err(ParseError::UndeclaredNets(nets));
                }
            }
        }

        self.check_net_names_in_continous_assignments()?;

        let port_widths: HashMap<_, _> = v_module
            .ports
            .iter()
            .map(|port| match port {
                Port::Ref(i) => (i.name(), signal_width(i.name())),
                Port::Named(_) => {
                    panic!("Port definition should not be a named port connection.")
                }
            })
            .collect();
        port_widths_by_module.insert(v_module_name, port_widths);
        port_names_by_module.insert(v_module_name, port_names);
        let net_ids_by_name = {
            let mut net_id_counter = (2..).into_iter().peekable();
            let net_ids_by_name: HashMap<_, _> = all_net_declarations
                .iter()
                .map(|&(slice, name)| {
                    // Expand a slice into a list of net IDs
                    // or create a single net ID if it is not an array.
                    let indices = slice
                        .map(|s| s.indices_range().collect_vec())
                        .unwrap_or(vec![0]);
                    // Generate net IDs.
                    let nets: Vec<usize> = indices
                        .iter()
                        .map(|_| net_id_counter.next().unwrap())
                        .collect();

                    (name, nets)
                })
                .collect();
            log::trace!(
                "Number of expanded nets: {}",
                *net_id_counter.peek().unwrap() - 2
            );
            net_ids_by_name
        };

        let mut bitwidth_mismatches = Vec::new();

        // Construct a net-to-net mapping according to the continous assigns.
        let continuous_assign_map = {
            let mut continuous_assign_map: Vec<(usize, usize)> = vec![];
            for ast::Assignment(lvalue, rexpr) in v_module.continuous_assignments() {
                let lvalues = lvalue.flat_concatenation();
                let rvalues = rexpr.flat_concatenation();

                // Convert to net IDs.
                let mut l_nets = vec![];
                for l in lvalues {
                    let ids = find_net_ids(&net_ids_by_name, &l.to_expr())?;
                    l_nets.extend(ids.into_iter());
                }

                // Convert to net IDs.
                let mut r_nets = vec![];
                for r in rvalues {
                    let ids = find_net_ids(&net_ids_by_name, r)?;
                    r_nets.extend(ids.into_iter());
                }

                // Bit length must be the same for both sides.
                if l_nets.len() != r_nets.len() {
                    bitwidth_mismatches.push((lvalue.clone(), rexpr.clone()));
                }

                let assign_map = l_nets.into_iter().zip(r_nets);
                continuous_assign_map.extend(assign_map);
            }
            continuous_assign_map
        };

        if !bitwidth_mismatches.is_empty() {
            return Err(ParseError::AssignmentWidthMismatch(bitwidth_mismatches));
        }
        let expanded_net_names: HashMap<usize, String> = all_net_declarations
            .iter()
            .flat_map(|&(_slice, net_name)| {
                let net_ids = &net_ids_by_name[net_name];
                let is_single_bit = net_ids.len() == 1;
                net_ids.iter().enumerate().map(move |(i, &id)| {
                    // Create a name for the bit of the expanded array.
                    let expanded_name = if is_single_bit {
                        debug_assert_eq!(i, 0);
                        net_name.clone()
                    } else {
                        // Append index like `signal_name.0`.
                        format!("{}.{}", net_name, i)
                    };
                    (id, expanded_name)
                })
            })
            .collect();
        let pins_with_net: Vec<_> = v_module
            .ports
            .iter()
            .flat_map(|port| {
                let port_name = match port {
                    Port::Ref(id) => id.name(),
                    Port::Named(_) => panic!("Expected a simple identifier."),
                };

                let direction = match io_net_directions_by_name[port_name] {
                    ast::Direction::Input => Direction::Input,
                    ast::Direction::Output => Direction::Output,
                    ast::Direction::InOut => Direction::InOut,
                };

                let net_ids = &net_ids_by_name[port_name];

                let _expanded_net_names = &expanded_net_names;

                net_ids.iter().map(move |id| {
                    let name = &_expanded_net_names[id];
                    (port_name, name, direction, id)
                })
            })
            .collect();
        let circuit_id = netlist.create_cell(v_module_name.clone().into());
        for &(_port_name, name, direction, _id) in &pins_with_net {
            netlist.create_pin(&circuit_id, name.clone().into(), direction);
        }
        let module_pin_expansion: HashMap<&String, Vec<usize>> = pins_with_net
            .iter()
            .enumerate()
            .map(|(pin_position, &(port_name, _, _, _))| (port_name, pin_position))
            .into_group_map();
        pin_expansion.insert(v_module_name, module_pin_expansion);
        Ok(if !self.load_blackboxes {
            // Optionally skip loading of the module content.
            // Create net objects that reside in the namespace of the parent circuit.
            let nets_by_id: HashMap<usize, N::NetId> = {
                let mut nets_by_id: HashMap<_, _> = expanded_net_names
                    .iter()
                    .map(|(&id, net_name)| {
                        // Try if net already exists (for __HIGH__ and __LOW__).
                        let net = netlist
                            .net_by_name(&circuit_id, net_name.as_str())
                            .unwrap_or_else(
                                // Otherwise create the net.
                                || netlist.create_net(&circuit_id, Some(net_name.clone().into())),
                            );
                        (id, net)
                    })
                    .collect();

                nets_by_id.insert(0, netlist.net_zero(&circuit_id));
                nets_by_id.insert(1, netlist.net_one(&circuit_id));
                nets_by_id
            };

            // Connect all pins of the circuit to the internal nets.
            netlist
                .each_pin_vec(&circuit_id)
                .iter()
                .zip(pins_with_net)
                .for_each(|(pin_id, (_, name, _, net_id))| {
                    {
                        let pin_name = netlist.pin_name(&pin_id);
                        let pin_name_str: &String = pin_name.borrow();
                        debug_assert_eq!(name, pin_name_str, "Pin names don't match.");
                    }
                    netlist.connect_pin(pin_id, Some(nets_by_id[net_id].clone()));
                });

            // Make module instances and connections to them.
            for (v_template_name, v_inst) in v_module.instantiations() {
                // Name of the instance that will be created now.
                let v_inst_name = v_inst.0.name();

                // Find circuit by name.
                let leaf_circuit = netlist
                    .cell_by_name(v_template_name.as_str())
                    // Existence of all required circuits should be verified already.
                    .expect("Circuit not found by name.");

                log::trace!(
                    "Create instance of '{}' in module '{}'.",
                    v_template_name,
                    v_module_name
                );

                // Create sub-circuit instance.
                let inst = netlist.create_cell_instance(
                    &circuit_id,
                    &leaf_circuit,
                    Some(v_inst_name.clone().into()),
                );

                // Loop over pin instances and attach them to the nets.
                let all_pin_instances = netlist.each_pin_instance_vec(&inst);

                let port_connections = &v_inst.1;

                if let Some(v_template) = modules_by_name.get(v_template_name).copied() {
                    // The template module was was found and hence is defined in the current verilog file.
                    // Therefore we know the port definitions.

                    // Number of ports that are required for this template.
                    let num_ports_expected = v_template.ports.len();

                    // Find out what ports need to be connected to which nets.
                    let port_connections_by_name: HashMap<_, _> = match port_connections {
                        ModulePortConnections::Unnamed(conn) => {
                            // Connections by position.

                            // Check that the number of connections matches with the number of ports.
                            let num_ports_actual = conn.len();
                            if num_ports_actual != num_ports_expected {
                                log::error!("Mismatch in number of unnamed ports in module '{}', instantiation of '{}'. Found {}, expected {}.",
                                            v_template_name, v_inst_name, num_ports_actual, num_ports_expected);
                                return Err(ParseError::PortConnectionMismatch);
                            }

                            // Check that the number of previously stored port names is consistent
                            // with the number of expected ports.
                            debug_assert_eq!(
                                port_names_by_module[v_template_name].len(),
                                num_ports_expected
                            );

                            // Create (port name, connection) tuples.
                            port_names_by_module[v_template_name]
                                .iter()
                                .zip(conn)
                                .map(|(&port_name, conn)| (port_name, conn))
                                .collect()
                        }
                        ModulePortConnections::Named(conn) => {
                            // Connections by port name.
                            conn.iter()
                                .map(|ast::NamedPortConnection(name, expr)| (name.name(), expr))
                                .collect()
                        }
                    };

                    {
                        // Sanity check on number of port connections. Each port must be connected.
                        let num_ports_actual = port_connections_by_name.len();
                        if num_ports_actual != num_ports_expected {
                            log::warn!("Mismatch in number of ports in module '{}', instantiation of '{}'. Found {}, expected {}.",
                                       v_template_name, v_inst_name, num_ports_actual, num_ports_expected);
                            // return Err(ParseError::PortConnectionMismatch);
                        }
                    }

                    {
                        // Sanity check: bit-width of the port assignments must match.
                        for (&port_name, &expr) in &port_connections_by_name {
                            let bitwidth = *port_widths_by_module[v_template_name]
                                .get(port_name)
                                .ok_or_else(|| {
                                super::ParseError::NoSuchPort(
                                    v_template_name.clone(),
                                    port_name.clone(),
                                )
                            })?;

                            // Expand the expression into bit signals and get the associated
                            // nets.
                            let mut nets: Vec<&N::NetId> = vec![];
                            for expr in expr.flat_concatenation()
                            // Expand concatenations.
                            {
                                let ids = find_net_ids(&net_ids_by_name, expr)?;
                                let current_nets =
                                    ids.into_iter().map(|net_id| &nets_by_id[&net_id]);
                                nets.extend(current_nets)
                            }

                            if nets.len() != bitwidth {
                                log::error!("Bit-width mismatch of port connection '{}' of instance '{}' in module '{}'.",
                                            port_name, v_inst_name, v_module_name);
                                return Err(ParseError::PortConnectionWidthMismatch);
                            }
                        }
                    }

                    // Connect instance ports.
                    for (port_name, expr) in port_connections_by_name {
                        // Convert the expression into nets.
                        let flat_net_ids = find_net_ids(&net_ids_by_name, expr)?;
                        let flat_nets = flat_net_ids
                            .iter()
                            .map(|id| nets_by_id[id].clone())
                            .collect_vec();

                        // Find pin positions of this port.
                        let pin_positions = &*pin_expansion[v_template_name][port_name];

                        let expected_bit_width = pin_positions.len();
                        let actual_bit_width = flat_nets.len();

                        // Check that the bit lengths match.
                        if actual_bit_width != expected_bit_width {
                            log::error!("Bit width mismatch in port connection '{}' of instance '{}' in module '{}'.",
                                        port_name, v_inst_name, v_module_name);
                            return Err(ParseError::PortConnectionWidthMismatch);
                        }

                        // Connect the pin instances with the nets.
                        for (&pin_position, net) in pin_positions.into_iter().zip(flat_nets) {
                            let pin_inst = &all_pin_instances[pin_position];
                            netlist.connect_pin_instance(pin_inst, Some(net));
                        }
                    }
                } else {
                    // Module was already present in the netlist.
                    // Since the netlist does not support yet the concept of ports, we have
                    // to connect to the flat pin list.

                    let num_connections_expected = all_pin_instances.len();

                    match port_connections {
                        ModulePortConnections::Unnamed(conn) => {
                            // Connections by position.

                            let mut flat_nets: Vec<&N::NetId> = vec![];
                            for expr in Expr::Concatenation(conn.clone())
                                .flat_concatenation() // Expand concatenations.
                                .into_iter()
                            {
                                // Translate into net IDs.
                                let ids = find_net_ids(&net_ids_by_name, &expr)?;
                                let nets = ids.into_iter().map(|net_id| &nets_by_id[&net_id]);
                                flat_nets.extend(nets);
                            }

                            // Check that the number of connections matches with the number of ports.
                            let num_connections_actual = flat_nets.len();
                            if num_connections_actual != num_connections_expected {
                                log::error!("Mismatch in number of bit connections in module '{}', instantiation of '{}'. Found {}, expected {}.",
                                            v_template_name, v_inst_name, num_connections_actual, num_connections_expected);
                                return Err(ParseError::PortConnectionMismatch);
                            }

                            // Make connections.
                            for (pin_inst, net) in all_pin_instances.iter().zip_eq(flat_nets) {
                                netlist.connect_pin_instance(pin_inst, Some(net.clone()));
                            }
                        }
                        ModulePortConnections::Named(conn) => {
                            // Connections by port name.
                            // TODO FIXME: In this case the ordering of the nets is not guaranteed to be correct.
                            // The ordering of named connections can be different from the actual
                            // ordering of the ports.

                            // For each named connection find the module pin (by name) and
                            // attach it to the net.
                            // TODO: Only single-bit connections are supported.
                            for c in conn {
                                let conn_name = c.0.name();
                                let pin = netlist.pin_by_name(&leaf_circuit, conn_name);
                                if let Some(pin) = pin {
                                    // Flatten the expression in the port connection.
                                    let flat_nets: Vec<_> = find_net_ids(&net_ids_by_name, &c.1)?
                                        .into_iter()
                                        .map(|net_id| &nets_by_id[&net_id])
                                        .collect();

                                    if flat_nets.len() != 1 {
                                        let msg = format!("Number of bits in named connection must be exactly 1\
                                                 when the module is \
                                                defined in another file: port '{}', Instance '{}' of '{}' in '{}'",
                                                          conn_name,
                                                          v_inst_name,
                                                          v_template_name,
                                                          v_module_name);
                                        log::error!("{}", &msg);
                                        return Err(ParseError::UndefinedError(msg));
                                    }

                                    // Connect the pin to the net.
                                    let net = flat_nets[0];
                                    let pin_inst = netlist.pin_instance(&inst, &pin);
                                    netlist.connect_pin_instance(&pin_inst, Some(net.clone()));
                                } else {
                                    let msg = format!(
                                        "No such pin in cell '{}': {}",
                                        v_module_name, conn_name
                                    );
                                    log::error!("{}", &msg);
                                    return Err(ParseError::UndefinedError(msg));
                                }
                            }
                        }
                    }
                }

                // Expand the ports.
                // let flat: Vec<_> = conn.iter()
                //     .flat_map(|e| e.flat_concatenation().into_iter())
                //     .collect();
                // if all_template_pins.len() != flat.len() {
                //     log::error!("Bit width mismatch in port connection of instance {}. Found {}, expected {}.",
                //                v_inst_name, flat.len(), all_template_pins.len());
                // }
            }

            // Process continuous assign statements.
            // Replace all left-hand side nets by the right-hand side nets.
            {
                let mut net_replacements = HashMap::new();
                for (left, right) in &continuous_assign_map {
                    let mut left_net = nets_by_id[left].clone();
                    while let Some(resolved) = net_replacements.get(&left_net).cloned() {
                        left_net = resolved;
                    }
                    let mut right_net = nets_by_id[right].clone();
                    while let Some(resolved) = net_replacements.get(&right_net).cloned() {
                        right_net = resolved;
                    }
                    netlist.replace_net(&left_net, &right_net);
                    net_replacements.insert(left_net, right_net);
                }
            }
        })
    }

    fn build_module_dependency_graph<'a>(
        &self,
        modules: &'a Vec<Module>,
    ) -> GraphMap<&'a String, (), petgraph::prelude::Directed> {
        log::debug!("Build module dependency graph.");
        let load_blackboxes = self.load_blackboxes;
        let mut dependency_graph: petgraph::graphmap::DiGraphMap<&String, ()> = modules
            .iter()
            .flat_map(|v_module| {
                let module_name = v_module.name.name();
                // Find all modules that get instanciated in this module.
                v_module
                    .instantiations()
                    .filter(|_| !load_blackboxes) // Ignore instances if flag is set.
                    .map(move |(inst_module_name, _)| (inst_module_name, module_name))
            })
            .collect();
        // Add all modules as nodes. Some might not have appeared in a dependency relation.
        modules.iter().for_each(|v_module| {
            dependency_graph.add_node(v_module.name.name());
        });
        dependency_graph
    }

    fn check_net_names_in_continous_assignments(&self) -> Result<(), ParseError> {
        // TODO
        Ok(())
    }
}

fn check_missing_modules<N: NetlistBase>(
    topo_sorted: &Vec<&String>,
    modules_by_name: &HashMap<&String, &Module>,
    netlist: &mut N,
) -> Result<(), ParseError> {
    log::debug!("Check that all required modules are present.");
    let missing_modules: Vec<String> = topo_sorted
        .iter()
        .filter(|&name| {
            !modules_by_name.contains_key(name) && !netlist.cell_by_name(name.as_str()).is_some()
        })
        .map(|&n| n.clone())
        .collect();
    if !missing_modules.is_empty() {
        log::error!("Modules are missing: {}", missing_modules.iter().join(", "));
        return Err(ParseError::MissingModules(missing_modules));
    }
    Ok(())
}

fn topo_sort(
    dependency_graph: GraphMap<&String, (), petgraph::prelude::Directed>,
) -> Result<Vec<&String>, ParseError> {
    let topo = petgraph::algo::toposort(&dependency_graph, None);
    let topo_sorted: Vec<&String> = match topo {
        Ok(sorted) => sorted,
        Err(cycle) => {
            log::error!("Modules have a cyclic dependency. ({})", cycle.node_id());
            return Err(ParseError::CyclicDependency);
        }
    };
    Ok(topo_sorted)
}

/// Given a verilog expression (concatenation, identifier (optionally sliced or indexed)
/// find the corresponding net IDs.
/// Constant integers are translated into a vector of `[0, 1]` with a length
/// corresponding to the bit width of the integer literal.
///
/// Returns an error if a net is not declared.
fn find_net_ids(
    lookup_table: &HashMap<&String, Vec<usize>>,
    expr: &Expr,
) -> Result<Vec<usize>, ParseError> {
    let lut = |n: &String| {
        lookup_table
            .get(n)
            .ok_or_else(|| ParseError::UndeclaredNets(vec![n.clone()]))
    };

    let mut ids = vec![];

    for expr in expr.flat_concatenation() {
        match expr {
            Expr::Ref(i) => ids.extend(lut(i.name())?),
            Expr::IdentIndexed(i) => ids.push(lut(i.name())?[i.index()]),

            Expr::IdentSliced(i) => {
                let iter = &lut(i.name())?[i.lowest_index()..=i.highest_index()];
                ids.extend(iter);
            }
            Expr::Num(n) => n.to_bits_msb_first().iter().for_each(|&b| match b {
                Bit::Zero => ids.push(0),
                Bit::One => ids.push(1),
                Bit::DontCare => ids.push(0), // Set don't-cares to a constant. TODO: Is this correct?
                Bit::HighImpedance => {
                    unimplemented!("High-Z nets are not supported.")
                }
            }),
            Expr::Concatenation(_c) => {
                panic!("Flattened concatenation should not contain a concatenation.")
            }
        };
    }

    Ok(ids)
}

impl NetlistReader for StructuralVerilogReader {
    type Error = ParseError;

    /// Parse a verilog netlist.
    fn read_into_netlist<R: Read, N: NetlistEdit>(
        &self,
        reader: &mut R,
        netlist: &mut N,
    ) -> Result<(), Self::Error> {
        // Read all data into memory.
        log::debug!("Read verilog netlist.");

        let verilog_data: Vec<u8> = reader.bytes().map(|b| b.unwrap()).collect();

        let verilog_string = String::from_utf8(verilog_data);

        if let Ok(verilog_string) = verilog_string {
            let parser = grammar::NetlistParser::new();

            log::debug!("Parse structural verilog.");
            let result: Result<Vec<Module>, _> = parser.parse(verilog_string.as_str());

            if let Ok(result) = result {
                self.convert_ast_to_netlist(result, netlist)?;
                Ok(())
            } else {
                log::error!("Error while reading verilog: {:?}", &result);
                let msg = match result {
                    Err(lalrpop_util::ParseError::UnrecognizedToken {
                        token,
                        expected: exp,
                    }) => {
                        let (start, token, _end) = token;
                        // Find line number of the error.
                        let line_num = 1 + verilog_string
                            .chars()
                            .take(start)
                            .filter(|&c| c == '\n')
                            .count();
                        let msg = format!(
                            "Unrecognized token '{}' on line {}. Expected one of {}.",
                            token,
                            line_num,
                            exp.join(", ")
                        );
                        log::error!("{}", msg);
                        msg
                    }
                    Err(lalrpop_util::ParseError::InvalidToken { location }) => {
                        // Find line number of the error.
                        let line_num = 1 + verilog_string
                            .chars()
                            .take(location)
                            .filter(|&c| c == '\n')
                            .count();
                        let msg = format!("Invalid token on line {}.", line_num);
                        log::error!("{}", msg);
                        msg
                    }
                    _ => "Undefined error".to_string(),
                };

                Err(ParseError::InvalidVerilog(msg))
            }
        } else {
            Err(ParseError::InvalidUTF8)
        }
    }
}
