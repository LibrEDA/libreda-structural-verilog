// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#![cfg(test)]

use libreda_db::prelude::{HierarchyBase, NetlistBase, NetlistReader, NetlistWriter};
use libreda_structural_verilog::{StructuralVerilogReader, StructuralVerilogWriter};

use libreda_db::chip::Chip;
use std::fs::File;

#[test]
fn test_read_netlist() {
    let library_reader = StructuralVerilogReader::new().load_blackboxes(true);
    let reader = StructuralVerilogReader::new();

    let mut f_lib = File::open("./tests/test_data/standard_cells.v").unwrap();
    let mut f_netlist = File::open("./tests/test_data/my_chip_45_nl.v").unwrap();
    let mut netlist: Chip = library_reader.read_netlist(&mut f_lib).unwrap();

    let result = reader.read_into_netlist(&mut f_netlist, &mut netlist);

    assert!(result.is_ok())
}

#[test]
fn test_read_write_netlist() {
    let library_reader = StructuralVerilogReader::new().load_blackboxes(true);
    let reader = StructuralVerilogReader::new();

    let mut f_lib = File::open("./tests/test_data/standard_cells.v").unwrap();
    let mut f_netlist = File::open("./tests/test_data/my_chip_45_nl.v").unwrap();
    let mut netlist: Chip = library_reader.read_netlist(&mut f_lib).unwrap();

    let result = reader.read_into_netlist(&mut f_netlist, &mut netlist);

    assert!(result.is_ok());

    let writer = StructuralVerilogWriter::new();
    let mut f_out = File::create("./tests/test_data/my_chip_45_nl_out.v").unwrap();
    let result = writer.write_netlist(&mut f_out, &netlist);

    assert!(result.is_ok());
}

#[test]
fn test_pin_reordering() {
    let library_reader = StructuralVerilogReader::new().load_blackboxes(true);

    let reader = StructuralVerilogReader::new();

    let library_data = r#"
module INVX1 (Y, A);
    output Y;
    input A;

    INVX1 _0_ (
        .A(A),
        .Y(Y)
    );
endmodule
    "#;

    let data = r#"

module TOP();
    wire a;
    wire y;

    INVX1 inv_inst1 (
        .A(a),
        .Y(y)
    );
endmodule
    "#;
    let mut bytes = data.as_bytes();
    let mut library_bytes = library_data.as_bytes();

    let mut netlist: Chip = library_reader.read_netlist(&mut library_bytes).unwrap();

    // let mut netlist: Chip = reader.read_netlist(&mut (&mut library_bytes).chain(&mut bytes)).unwrap();

    reader.read_into_netlist(&mut bytes, &mut netlist).unwrap();

    let top_cell = netlist.cell_by_name("TOP").unwrap();
    let inv_cell = netlist.cell_by_name("INVX1").unwrap();
    let inv_inst = netlist
        .cell_instance_by_name(&top_cell, "inv_inst1")
        .unwrap();

    let pin_y = netlist.pin_by_name(&inv_cell, "Y").unwrap();

    assert_eq!(
        netlist.net_of_pin_instance(&netlist.pin_instance(&inv_inst, &pin_y)),
        netlist.net_by_name(&top_cell, "y")
    )
}

#[test]
fn test_assign_after_assign() {
    let library_reader = StructuralVerilogReader::new().load_blackboxes(true);

    let reader = StructuralVerilogReader::new();

    let library_data = r#"
module INVX1 (Y, A);
    output Y;
    input A;

    INVX1 _0_ (
        .A(A),
        .Y(Y)
    );
endmodule
    "#;

    let data = r#"

module TOP();
    wire a;
    wire y;
    wire b;
    wire c;

    INVX1 inv_inst1 (
        .A(b),
        .Y(y)
    );

    assign c = a;
    assign b = c;
endmodule
    "#;
    let mut bytes = data.as_bytes();
    let mut library_bytes = library_data.as_bytes();

    let mut netlist: Chip = library_reader.read_netlist(&mut library_bytes).unwrap();

    reader.read_into_netlist(&mut bytes, &mut netlist).unwrap();

    let top_cell = netlist.cell_by_name("TOP").unwrap();
    let inv_cell = netlist.cell_by_name("INVX1").unwrap();
    let inv_inst = netlist
        .cell_instance_by_name(&top_cell, "inv_inst1")
        .unwrap();

    let pin_a = netlist.pin_by_name(&inv_cell, "A").unwrap();

    assert_eq!(
        netlist.net_of_pin_instance(&netlist.pin_instance(&inv_inst, &pin_a)),
        netlist.net_by_name(&top_cell, "a")
    )
}

#[test]
fn test_undeclared_net_in_continous_assign() {
    // Should handle this case gracefully.
    let reader = StructuralVerilogReader::new();

    let data = r#"
module TOP();
    wire a;
    
    assign xx = a;
endmodule
    "#;
    let mut bytes = data.as_bytes();

    let result: Result<Chip, _> = reader.read_netlist(&mut bytes);
    assert!(result.is_err());
}
